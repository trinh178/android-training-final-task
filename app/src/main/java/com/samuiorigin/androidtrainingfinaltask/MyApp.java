package com.samuiorigin.androidtrainingfinaltask;

import android.app.Application;
import android.util.Log;

import com.samuiorigin.androidtrainingfinaltask.dagger.AppComponent;
import com.samuiorigin.androidtrainingfinaltask.dagger.DaggerAppComponent;

import io.realm.Realm;

public class MyApp extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Realm
        Realm.init(this);

        // Dagger
        appComponent = DaggerAppComponent
                .builder()
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
