package com.samuiorigin.androidtrainingfinaltask.base;

import android.support.v7.app.AppCompatActivity;

import com.samuiorigin.androidtrainingfinaltask.MyApp;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
        ((MyApp) getApplication()).getAppComponent().inject(this);
    }
}
