package com.samuiorigin.androidtrainingfinaltask.base;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;
import com.samuiorigin.androidtrainingfinaltask.MyApp;

import butterknife.ButterKnife;

public abstract class BaseMvpActivity<V extends MvpView, P extends MvpPresenter<V>>
                             extends MvpActivity<V, P> {
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
        ((MyApp) getApplication()).getAppComponent().inject(presenter);
    }
}
