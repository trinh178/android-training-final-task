package com.samuiorigin.androidtrainingfinaltask.calendar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.base.BaseMvpFragment;
import com.samuiorigin.androidtrainingfinaltask.calendar.adapter.CalendarVPAdapter;
import com.samuiorigin.androidtrainingfinaltask.calendar.listener.CalendarOnDragListener;
import com.samuiorigin.androidtrainingfinaltask.calendar.listener.CalendarOnPageChangeListener;
import com.samuiorigin.androidtrainingfinaltask.calendar.listener.TimelineOnTouchListener;
import com.samuiorigin.androidtrainingfinaltask.event.EventActivity;
import com.samuiorigin.androidtrainingfinaltask.model.Event;
import com.samuiorigin.androidtrainingfinaltask.utils.Time;
import com.samuiorigin.androidtrainingfinaltask.utils.Utils;

import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;

public class CalendarFragment extends BaseMvpFragment<CalendarView, CalendarPresenter>
                              implements CalendarView {
    // Const
    public static final int HOURS = 24;
    private static final float DEF_TIMELINE_HOUR_SIZE = 100f;
    private static final float MIN_TIMELINE_ZOOM_FACTOR = 0.5f;
    private static final float MAX_TIMELINE_ZOOM_FACTOR = 2f;
    //public static final int TIMELINE_FOOTER_WIDTH = 100;

    @BindView(R.id.vpCalendar) public ViewPager vpCalendar;

    // Params
    private float curTimeLineZoomFactor;

    public CalendarVPAdapter adapter;

    // Current time
    Calendar curCalendar;

    // Timeline
    @BindView(R.id.svTimeline) ScrollView svTimeline;
    @BindView(R.id.layoutTimeline) RelativeLayout layoutTimeline;
    @BindView(R.id.vNowLine) View vNowLine;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_calendar;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Params
        curTimeLineZoomFactor = 1f;

        // Current time
        curCalendar = Calendar.getInstance();

        initCalendar();
        initTimeline();

        //
        presenter.onStart();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initTimeline() {
        // View
        for (int i = HOURS - 1; i >= 0; i--) {
            View view = getLayoutInflater().inflate(R.layout.calendar_timeline, layoutTimeline, false);
            ((TextView) view.findViewById(R.id.tvTime)).setText(String.valueOf(i));
            layoutTimeline.addView(view, 0);
        }
        updateTimelineLayoutRender(getCurTimeLineHourSizeInPixel(), Utils.getTime(curCalendar));

        // Touch event
        svTimeline.setOnTouchListener(new TimelineOnTouchListener(this));
        // Drag event
        vpCalendar.setOnDragListener(new CalendarOnDragListener(this));
    }

    private void initCalendar() {
        Calendar[] calendars = new Calendar[] {
                (Calendar) curCalendar.clone(),
                (Calendar) curCalendar.clone(),
                (Calendar) curCalendar.clone()
        };
        calendars[0].set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendars[1].set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendars[2].set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendars[0].add(Calendar.DATE, -7);
        calendars[2].add(Calendar.DATE, 7);

        adapter = new CalendarVPAdapter(this, calendars);

        vpCalendar.setAdapter(adapter);

        vpCalendar.setCurrentItem(1);

        vpCalendar.addOnPageChangeListener(new CalendarOnPageChangeListener(this));
    }

    // Params
    public void addCurTimeLineZoomFactor(float factor) {
        this.curTimeLineZoomFactor += factor;
        if (this.curTimeLineZoomFactor < MIN_TIMELINE_ZOOM_FACTOR) {
            this.curTimeLineZoomFactor = MIN_TIMELINE_ZOOM_FACTOR;
        } else if (this.curTimeLineZoomFactor > MAX_TIMELINE_ZOOM_FACTOR) {
            this.curTimeLineZoomFactor = MAX_TIMELINE_ZOOM_FACTOR;
        }
    }
    public float getCurTimeLineHourSize() {
        return DEF_TIMELINE_HOUR_SIZE * this.curTimeLineZoomFactor;
    }
    public int getCurTimeLineHourSizeInPixel() {
        float dp = getCurTimeLineHourSize();
        return (int)(dp * ((float) Objects.requireNonNull(getContext()).getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void setTimelineScrollY(int y) {
        svTimeline.post(()->svTimeline.setScrollY(y));
    }
    public void syncTimelineZoom() {
        int px = getCurTimeLineHourSizeInPixel();
        updateTimelineLayoutRender(px, Utils.getTime(curCalendar));
    }
    // Resize
    public void updateTimelineLayoutRender(int hourWidth, Time time) {
        // Size
        ViewGroup.LayoutParams layoutParams = layoutTimeline.getLayoutParams();
        layoutParams.height = hourWidth * HOURS;
        layoutTimeline.setLayoutParams(layoutParams);

        for (int i = 0; i < layoutTimeline.getChildCount() - 1; i++) {
            View view = layoutTimeline.getChildAt(i);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
            params.topMargin = hourWidth * i;
            view.setLayoutParams(params);
        }
        // Now time
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) vNowLine.getLayoutParams();
        params.topMargin = (int) ((time.getHour() + time.getMinute()/60f) * hourWidth);
        vNowLine.setLayoutParams(params);
    }

    // Current time
    public Calendar getCurrentCalendar() {
        return curCalendar;
    }


    // Mvp
    @NonNull
    @Override
    public CalendarPresenter createPresenter() {
        return new CalendarPresenter();
    }
    @Override
    public void setNowTime(Calendar cal) {
        curCalendar = cal;
        adapter.syncNowTimeline();
    }
    @Override
    public void focusNowTime() {
        // FOCUS IN DAYS
        // This is the show week
        Calendar showCal = adapter.getCurrentPageCalendar();
        // This is the now week that we want show
        Calendar cal = (Calendar) curCalendar.clone();

        // Set to First day of each week
        showCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        // Difference
        int dif = Utils.differenceBetweenDates(showCal, cal);

        if (dif > 0) { // Next to now time
            cal.add(Calendar.DATE, -7);
            adapter.setAndNotifyGotoWeekData(cal.getTime());
            vpCalendar.setCurrentItem(2);
        } else if (dif < 0) { // Previous to now time
            cal.add(Calendar.DATE, 7);
            adapter.setAndNotifyGotoWeekData(cal.getTime());
            vpCalendar.setCurrentItem(0);
        }

        // FOCUS ON TIMELINE
        int px = getCurTimeLineHourSizeInPixel() * curCalendar.get(Calendar.HOUR_OF_DAY);
        adapter.setTimeLineScrollY(px);

        // ANIMATION
        adapter.animateNowDateView();
    }
    @Override
    public void previousWeek() {
        vpCalendar.setCurrentItem(0);
    }
    @Override
    public void nextWeek() {
        vpCalendar.setCurrentItem(1);
    }
    @Override
    public void previousMonth() {
        // This is the show week
        Calendar showCal = adapter.getCurrentPageCalendar();
        // This is the now week that we want show
        Calendar cal = (Calendar) adapter.getCurrentPageCalendar().clone();
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        // Set to First day of each week
        showCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        // Difference
        int dif = Utils.differenceBetweenDates(showCal, cal);

        if (dif > 0) { // Next to now time
            cal.add(Calendar.DATE, -7);
            adapter.setAndNotifyGotoWeekData(cal.getTime());
            vpCalendar.setCurrentItem(2);
        } else if (dif < 0) { // Previous to now time
            cal.add(Calendar.DATE, 7);
            adapter.setAndNotifyGotoWeekData(cal.getTime());
            vpCalendar.setCurrentItem(0);
        }
    }
    @Override
    public void nextMonth() {
        // This is the show week
        Calendar showCal = adapter.getCurrentPageCalendar();
        // This is the now week that we want show
        Calendar cal = (Calendar) adapter.getCurrentPageCalendar().clone();
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        // Set to First day of each week
        showCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        // Difference
        int dif = Utils.differenceBetweenDates(showCal, cal);

        if (dif > 0) { // Next to now time
            cal.add(Calendar.DATE, -7);
            adapter.setAndNotifyGotoWeekData(cal.getTime());
            vpCalendar.setCurrentItem(2);
        } else if (dif < 0) { // Previous to now time
            cal.add(Calendar.DATE, 7);
            adapter.setAndNotifyGotoWeekData(cal.getTime());
            vpCalendar.setCurrentItem(0);
        }
    }
    @Override
    public void openCreateEventScreen() {
        if (isAdded()) {
            Intent intent = new Intent(getActivity(), EventActivity.class);
            Objects.requireNonNull(getActivity()).startActivity(intent);
        }
    }
    @Override
    public void createEventView(Event event) {
        adapter.createEvent(event);
    }
    @Override
    public void deleteEventView(Event event) {
        adapter.removeEvent(event);
    }
    @Override
    public void updateEventView(Event event) {
        adapter.updateEvent(event);
    }

    // Event bus
    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }
}
