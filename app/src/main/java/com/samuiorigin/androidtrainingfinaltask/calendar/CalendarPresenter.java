package com.samuiorigin.androidtrainingfinaltask.calendar;

import android.os.AsyncTask;
import android.os.SystemClock;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.samuiorigin.androidtrainingfinaltask.eventbus.Message;
import com.samuiorigin.androidtrainingfinaltask.model.Event;
import com.samuiorigin.androidtrainingfinaltask.model.EventManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

@SuppressWarnings({"ConstantConditions", "unused"})
public class CalendarPresenter extends MvpBasePresenter<CalendarView> {
    private Calendar calendar;
    private NowTimeUpdatingTask nowTimeUpdatingTask;

    public void onStart() {
        if (isViewAttached()) {
            // Update
            calendar = Calendar.getInstance();
            getView().setNowTime(calendar);

            nowTimeUpdatingTask = new NowTimeUpdatingTask();
            nowTimeUpdatingTask.execute(this);
        }
    }
    void updateNowTime() {
        if (isViewAttached()) {
            getView().setNowTime(calendar);
        }
    }
    void updateNowTimeNextSecond() {
        if (isViewAttached()) {
            calendar.add(Calendar.SECOND, 1);
            getView().setNowTime(calendar);
        }
    }
    void updateNowTimeNextMinute() {
        if (isViewAttached()) {
            calendar.add(Calendar.MINUTE, 1);
            getView().setNowTime(calendar);
        }
    }
    void focusNowTime() {
        if (isViewAttached()) {
            getView().focusNowTime();
        }
    }
    void onCreateEvent() {
        if (isViewAttached()) {
            getView().openCreateEventScreen();
        }
    }
    void previousWeek() {
        if (isViewAttached()) {
            getView().previousWeek();
        }
    }
    void nextWeek() {
        if (isViewAttached()) {
            getView().nextWeek();
        }
    }

    public EventManager getEventManager() {
        return EventManager.getInstance();
    }

    // Event bus
    void onResume() {
        EventBus.getDefault().register(this);
    }
    void onPause() {
        EventBus.getDefault().unregister(this);
    }
    @Subscribe()
    public void onEvent(Message msg) {
        switch (msg.getAction()) {
            case Message.ACTION_FOCUS_NOW_TIME:
                if (isViewAttached()) {
                    getView().focusNowTime();
                }
                break;
            case Message.ACTION_PREVIOUS_WEEK:
                if (isViewAttached()) {
                    getView().previousMonth();
                }
                break;
            case Message.ACTION_NEXT_WEEK:
                if (isViewAttached()) {
                    getView().nextMonth();
                }
                break;
            case Message.ACTION_CREATE_EVENT:
                if (isViewAttached()) {
                    getView().openCreateEventScreen();
                }
                break;
            case Message.ACTION_DETAILS_EVENT:
                if (isViewAttached()) {
                    getView().openCreateEventScreen();
                }
                break;
            case Message.ACTION_CREATED_EVENT:
                if (isViewAttached()) {
                    Event event = (Event) msg.getData();
                    getView().createEventView(event);
                }
                break;
        }
    }
    @Subscribe(sticky = true)
    public void onEventSticky(Message msg) {
        switch (msg.getAction()) {
            case Message.ACTION_CREATED_EVENT:
                if (isViewAttached()) {
                    Event event = (Event) msg.getData();
                    getView().createEventView(event);
                }
                break;
            case Message.ACTION_DELETED_EVENT:
                if (isViewAttached()) {
                    Event event = (Event) msg.getData();
                    getView().deleteEventView(event);
                }
                break;
            case Message.ACTION_UPDATED_EVENT:
                if (isViewAttached()) {
                    Event event = (Event) msg.getData();
                    getView().updateEventView(event);
                }
                break;
        }
    }
}

class NowTimeUpdatingTask extends AsyncTask<CalendarPresenter, CalendarPresenter, Void> {
    @Override
    protected Void doInBackground(CalendarPresenter... calendarPresenters) {
        while (true) {
            SystemClock.sleep(1000);
            if (isCancelled()) {
                return null;
            }
            publishProgress(calendarPresenters[0]);
        }
    }
    @Override
    protected void onProgressUpdate(CalendarPresenter... calendarPresenters) {
        calendarPresenters[0].updateNowTimeNextSecond();
    }
}
