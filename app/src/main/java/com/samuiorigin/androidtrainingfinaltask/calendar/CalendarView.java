package com.samuiorigin.androidtrainingfinaltask.calendar;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.samuiorigin.androidtrainingfinaltask.model.Event;

import java.util.Calendar;

public interface CalendarView extends MvpView {
    void setNowTime(Calendar cal);
    void focusNowTime();
    void previousWeek();
    void nextWeek();
    void openCreateEventScreen();
    void createEventView(Event event);
    void deleteEventView(Event event);
    void updateEventView(Event event);
    void previousMonth();
    void nextMonth();
}
