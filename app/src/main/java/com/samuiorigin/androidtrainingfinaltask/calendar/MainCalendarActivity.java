package com.samuiorigin.androidtrainingfinaltask.calendar;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.base.BaseActivity;
import com.samuiorigin.androidtrainingfinaltask.menu.MenuFragment;

public class MainCalendarActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_calendar);

        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction trans = fm.beginTransaction();

        trans.add(R.id.container, new MenuFragment());
        trans.setCustomAnimations(android.R.anim.fade_out, android.R.anim.fade_in);
        trans.commit();

        trans = fm.beginTransaction();
        trans.add(R.id.container, new CalendarFragment());
        trans.commit();
    }
}
