package com.samuiorigin.androidtrainingfinaltask.calendar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.calendar.CalendarFragment;
import com.samuiorigin.androidtrainingfinaltask.calendar.adapter.listener.EventLayoutOnDragListener;
import com.samuiorigin.androidtrainingfinaltask.eventbus.Message;
import com.samuiorigin.androidtrainingfinaltask.model.Event;
import com.samuiorigin.androidtrainingfinaltask.model.EventManager;
import com.samuiorigin.androidtrainingfinaltask.utils.IDate;
import com.samuiorigin.androidtrainingfinaltask.utils.Time;
import com.samuiorigin.androidtrainingfinaltask.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@SuppressWarnings("unused")
public class CalendarVPAdapter extends PagerAdapter {
    public CalendarFragment thisFragment;
    private Calendar[] calendars;
    private LayoutInflater inflater;

    private DateFormat dateFormat;


    private int action; // -1: setAndNotifyPreviousWeekData, 1: setAndNotifyNextWeekData

    private int timeLineScrollY;

    private WeekViewHolder[] weekViewHolders;

    private View vNowTime;

    // Constructor
    public CalendarVPAdapter(CalendarFragment fragment, Calendar[] calendars) {
        Context context = fragment.getContext();
        this.thisFragment = fragment;
        this.calendars = calendars;
        inflater = LayoutInflater.from(context);
        Objects.requireNonNull(context);
        dateFormat = new SimpleDateFormat(context.getString(R.string.date_format),
                context.getResources().getConfiguration().getLocales().get(0));

        timeLineScrollY = 0;

        weekViewHolders = new WeekViewHolder[3];
    }

    // View paper
    @Override
    public int getCount() {
        return 3;
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return o == view;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (action == 0) {
            container.removeView((View) object);
        }
    }
    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if (action == -1) {
            if (position == 0) {
                WeekViewHolder weekViewHolder = createWeekView(container, calendars[position]);
                final ScrollView svEvents = weekViewHolder.svEvents;
                svEvents.post(() -> svEvents.setScrollY(timeLineScrollY));
                svEvents.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    timeLineScrollY = scrollY;
                    syncScrollState();
                });

                container.removeView(weekViewHolders[2].view);

                weekViewHolders[2] = weekViewHolders[1];
                weekViewHolders[1] = weekViewHolders[0];
                weekViewHolders[0] = weekViewHolder;

                container.addView(weekViewHolder.view);

                // Now
                setNowTimeViewRender(thisFragment.getCurrentCalendar());
            }
        } else if (action == 1) {
            if (position == 2) {
                WeekViewHolder weekViewHolder = createWeekView(container, calendars[position]);
                final ScrollView svEvents = weekViewHolder.svEvents;
                svEvents.post(() -> svEvents.setScrollY(timeLineScrollY));
                svEvents.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    timeLineScrollY = scrollY;
                    syncScrollState();
                });

                container.removeView(weekViewHolders[0].view);

                weekViewHolders[0] = weekViewHolders[1];
                weekViewHolders[1] = weekViewHolders[2];
                weekViewHolders[2] = weekViewHolder;

                container.addView(weekViewHolder.view);

                // Now
                setNowTimeViewRender(thisFragment.getCurrentCalendar());
            }
        } else {
            WeekViewHolder weekViewHolder = createWeekView(container, calendars[position]);
            final ScrollView svEvents = weekViewHolder.svEvents;
            svEvents.post(() -> svEvents.setScrollY(timeLineScrollY));
            svEvents.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                timeLineScrollY = scrollY;
                syncScrollState();
            });

            container.addView(weekViewHolder.view);

            weekViewHolders[position] = weekViewHolder;

            // Now
            setNowTimeViewRender(thisFragment.getCurrentCalendar());
        }
        return weekViewHolders[position].view;
    }

    private WeekViewHolder createWeekView(ViewGroup container, Calendar calendar) {
        WeekViewHolder weekViewHolder = new WeekViewHolder();

        View view = inflater.inflate(R.layout.calendar_pager, container, false);
        ViewGroup layoutDay = view.findViewById(R.id.layoutDay);

        weekViewHolder.view = view;
        weekViewHolder.svEvents = view.findViewById(R.id.svEvents);
        weekViewHolder.layoutDaysEvent = view.findViewById(R.id.layoutDaysEvent);

        Calendar cal = (Calendar) calendar.clone();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        //
        updateDaysEventLayoutRender(thisFragment.getCurTimeLineHourSizeInPixel());

        // Days
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.monday, Utils.getIDate(cal));
        cal.add(Calendar.DATE, 1);
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.tuesday, Utils.getIDate(cal));
        cal.add(Calendar.DATE, 1);
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.wednesday, Utils.getIDate(cal));
        cal.add(Calendar.DATE, 1);
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.thursday, Utils.getIDate(cal));
        cal.add(Calendar.DATE, 1);
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.friday, Utils.getIDate(cal));
        cal.add(Calendar.DATE, 1);
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.saturday, Utils.getIDate(cal));
        cal.add(Calendar.DATE, 1);
        addDayRender(layoutDay, weekViewHolder.layoutDaysEvent, R.string.sunday, Utils.getIDate(cal));

        return weekViewHolder;
    }
    private void addDayRender(ViewGroup dayContainer,
                              ViewGroup dayEventContainer,
                              @StringRes int dayName, IDate date) {
        // Day
        View view = inflater.inflate(R.layout.calendar_pager_day, dayContainer, false);
        ((TextView) view.findViewById(R.id.tvDay)).setText(dayName);
        ((TextView) view.findViewById(R.id.tvDate)).setText(date.toString(dateFormat));
        dayContainer.addView(view);

        // DaysEvent
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.calendar_pager_day_event, dayEventContainer, false);
        dayEventContainer.addView(viewGroup);

        // Events
        Event[] events = thisFragment.getPresenter().getEventManager().getEvents(
                date
        );
        ViewGroup layout = (ViewGroup) viewGroup.getChildAt(0);
        layout.setTag(date);
        for (Event event: events) {
            createEventRender(layout, event,
                    thisFragment.getCurTimeLineHourSizeInPixel());
        }

        layout.setOnDragListener(new EventLayoutOnDragListener(this));
    }

    //
    public void animateNowDateView() {
        Animation animation = AnimationUtils.loadAnimation(thisFragment.getContext(), R.anim.now_time);

        if (vNowTime != null) {
            vNowTime.startAnimation(animation);
        }
    }
    public Calendar getCurrentPageCalendar() {
        return (Calendar) calendars[1].clone();
    }
    public void setAndNotifyGotoWeekData(Date date) {
        calendars[0].setTime(date);
        calendars[1].setTime(date);
        calendars[2].setTime(date);
        calendars[0].add(Calendar.DATE, -7);
        calendars[2].add(Calendar.DATE, 7);
        action = 0;
        notifyDataSetChanged();
    }
    public void setAndNotifyPreviousWeekData() {
        calendars[0].add(Calendar.DATE, -7);
        calendars[1].add(Calendar.DATE, -7);
        calendars[2].add(Calendar.DATE, -7);
        action = -1;
        notifyDataSetChanged();
    }
    public void setAndNotifyNextWeekData() {
        calendars[0].add(Calendar.DATE, 7);
        calendars[1].add(Calendar.DATE, 7);
        calendars[2].add(Calendar.DATE, 7);
        action = 1;
        notifyDataSetChanged();
    }
    public void setTimeLineScrollY(int y) {
        timeLineScrollY = y;
        thisFragment.setTimelineScrollY(timeLineScrollY);
        for (WeekViewHolder weekViewHolder: weekViewHolders) {
            if (weekViewHolder != null) {
                weekViewHolder.svEvents.post(
                        () -> weekViewHolder.svEvents.smoothScrollTo(0, timeLineScrollY));
            }
        }
    }
    public void setTimelineScrollY(Time time) {
        int y = (int) (thisFragment.getCurTimeLineHourSizeInPixel() * time.getFloatHours());
        setTimeLineScrollY(y);
    }
    private void syncScrollState() {
        thisFragment.setTimelineScrollY(weekViewHolders[1].svEvents.getScrollY());
        weekViewHolders[0].svEvents.post(
                () -> weekViewHolders[0].svEvents.setScrollY(
                        weekViewHolders[1].svEvents.getScrollY()));
        weekViewHolders[2].svEvents.post(
                () -> weekViewHolders[2].svEvents.setScrollY(
                        weekViewHolders[1].svEvents.getScrollY()));
    }
    private void syncZoomState() {
        updateDaysEventLayoutRender(thisFragment.getCurTimeLineHourSizeInPixel());
        updateAllEventRender(thisFragment.getCurTimeLineHourSizeInPixel());
        thisFragment.syncTimelineZoom();
        syncNowTimeline();
    }
    public void zoomIn(float factor) {
        thisFragment.addCurTimeLineZoomFactor(factor);
        syncZoomState();
    }
    public void zoomOut(float factor) {
        thisFragment.addCurTimeLineZoomFactor(-factor);
        syncZoomState();
    }
    public void syncNowTimeline() {
        // Time line
        thisFragment.updateTimelineLayoutRender(
                thisFragment.getCurTimeLineHourSizeInPixel(),
                Utils.getTime(thisFragment.getCurrentCalendar())
        );
        // Now time
        updateNowTimeViewRender(Utils.getTime(thisFragment.getCurrentCalendar()));
    }

    // Event
    public void createEvent(Event event) {
        ViewGroup layout = getLayoutEvents(event);
        if (layout != null) {
            createEventRender(layout, event, thisFragment.getCurTimeLineHourSizeInPixel());
        }
    }
    public void removeEvent(Event event) {
        ViewGroup layout = getLayoutEvents(event);
        if (layout != null) {
            removeEventRender(layout, event);
        }
    }
    public void updateEvent(Event event) {
        View view = getEventView(event);
        if (view != null) {
            // Remove
            ((ViewGroup) view.getParent()).removeView(view);
            // Add
            createEvent(event);
        }
    }

    // Update render
    // Event layout
    private void updateDaysEventLayoutRender(int hourWidth) {
        int s = hourWidth * CalendarFragment.HOURS;
        for (WeekViewHolder holder : weekViewHolders) {
            if (holder != null) {
                ViewGroup.LayoutParams layoutParams = holder.layoutDaysEvent.getLayoutParams();
                layoutParams.height = s;
                holder.layoutDaysEvent.setLayoutParams(layoutParams);
            }
        }
    }
    // Now time
    private void updateNowTimeViewRender(Time time) {
        if (vNowTime != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) vNowTime.getLayoutParams();
            layoutParams.topMargin = (int) ((time.getHour() + time.getMinute()/60f) * thisFragment.getCurTimeLineHourSizeInPixel());
            vNowTime.setLayoutParams(layoutParams);
        }
    }
    private void setNowTimeViewRender(Calendar cal) {
        for (int i = 0; i < weekViewHolders.length; i ++) {
            WeekViewHolder holder = weekViewHolders[i];
            if (holder != null) {
                int dif = Utils.differenceBetweenDates(calendars[i],
                        thisFragment.getCurrentCalendar());
                if (dif >= 0 && dif < 7) {
                    if (vNowTime != null) {
                        vNowTime.setVisibility(View.INVISIBLE);
                    }
                    vNowTime = holder.layoutDaysEvent.getChildAt(dif).findViewById(R.id.vNow);
                    vNowTime.setVisibility(View.VISIBLE);
                    updateNowTimeViewRender(Utils.getTime(cal));
                    return;
                }
            }
        }
    }
    // Event
    private void createEventRender(ViewGroup layout, Event event, int hourWidth) {
        View view = inflater.inflate(R.layout.calendar_event, layout, false);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.height = (int) (hourWidth
                        * (event.getUptime().getHour() + (event.getUptime().getMinute() / 60f)));
        layoutParams.topMargin = (int) (hourWidth
                        * (event.getStartCalendar().get(Calendar.HOUR_OF_DAY) +
                        event.getStartCalendar().get(Calendar.MINUTE) / 60f));
        view.setLayoutParams(layoutParams);

        ((TextView) view.findViewById(R.id.tvTitle)).setText(event.getTitle());
        ((TextView) view.findViewById(R.id.tvStartTime)).setText(
                Utils.timeToString(thisFragment.getString(R.string.time_format),
                        event.getStartCalendar()));
        view.setTag(event);

        layout.addView(view);

        // Drag drop
        view.setOnLongClickListener(v -> {
            View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);
            v.startDragAndDrop(null, dragShadowBuilder, v, 0);
            return false;
        });
        // Details click
        view.setOnClickListener(v -> EventBus.getDefault().postSticky(new Message<>(
                Message.ACTION_DETAILS_EVENT,
                event
        )));
    }
    private void removeEventRender(ViewGroup layout, Event event) {
        View view = layout.findViewWithTag(event);
        if (view != null) {
            layout.removeView(view);
        }
    }
    private void updateAllEventRender(int hourWidth) {
        for (WeekViewHolder holder: weekViewHolders) {
            if (holder != null) {
                for (int i = 0; i < holder.layoutDaysEvent.getChildCount(); i++) {
                    ViewGroup viewGroup =
                            (ViewGroup) ((ViewGroup) holder.layoutDaysEvent.getChildAt(i)).getChildAt(0);
                    for (int j = 0; j < viewGroup.getChildCount(); j ++) {
                        View view = viewGroup.getChildAt(j);
                        Event event = (Event) view.getTag();
                        RelativeLayout.LayoutParams layoutParams =
                                (RelativeLayout.LayoutParams) view.getLayoutParams();
                        layoutParams.height = (int) (hourWidth
                                * (event.getUptime().getHour() + (event.getUptime().getMinute() / 60f)));
                        layoutParams.topMargin = (int) (hourWidth
                                * (event.getStartCalendar().get(Calendar.HOUR_OF_DAY) +
                                event.getStartCalendar().get(Calendar.MINUTE) / 60f));
                        view.setLayoutParams(layoutParams);
                    }
                }
            }
        }
    }
    private void updateEventContentRender(Event event) {
        ViewGroup view = (ViewGroup) ((ViewGroup) Objects.requireNonNull(getEventView(event))).getChildAt(0);
        ((TextView) view.getChildAt(0)).setText(event.getTitle());
        view = (ViewGroup) view.getChildAt(1);
        ((TextView) view.getChildAt(1)).setText(Utils.timeToString(
                thisFragment.getString(R.string.time_format),
                event.getStartTime()
        ));
    }

    // Operator
    public boolean IsCalendarRendering(Calendar cal) {
        if (calendars[0] == null ||
                calendars[1] == null ||
                calendars[2] == null) return false;
        int diff = Utils.differenceBetweenDates(calendars[0], cal);
        return diff > 0 && diff < 21;
    }
    public WeekViewHolder getViewContainCalendar(Calendar cal) {
        for (int i = 0; i < 3; i++) {
            if (weekViewHolders[i] != null) {
                int diff = Utils.differenceBetweenDates(calendars[i], cal);
                if (diff > 0 && diff < 7) {
                    return weekViewHolders[i];
                }
            }
        }
        return null;
    }
    private ViewGroup getLayoutEvents(Event event) {
        for (int i = 0; i < 3; i++) {
            if (weekViewHolders[i] != null) {
                int diff = Utils.differenceBetweenDates(calendars[i], event.getStartCalendar());
                if (diff >= 0 && diff < 7) {
                    return (ViewGroup) ((ViewGroup) weekViewHolders[i].layoutDaysEvent.getChildAt(diff)).getChildAt(0);
                }
            }
        }
        return null;
    }
    private View getEventView(Event event) {
        for (WeekViewHolder weekViewHolder: weekViewHolders) {
            if (weekViewHolder != null) {
                View view = weekViewHolder.view.findViewWithTag(event);
                if (view != null) {
                    return view;
                }
            }
        }
        return null;
    }

    // Listener
    public void OnEventMoveEnded(Event event, int newPos, IDate newDate) {
        EventManager eventManager = thisFragment.getPresenter().getEventManager();

        int hour = newPos / thisFragment.getCurTimeLineHourSizeInPixel();
        int minute = (int) (((newPos % thisFragment.getCurTimeLineHourSizeInPixel())
                        / (float)thisFragment.getCurTimeLineHourSizeInPixel())
                        * 60);
        Time uptime = event.getUptime();
        event.getStartCalendar().set(Calendar.HOUR_OF_DAY, hour);
        event.getStartCalendar().set(Calendar.MINUTE, minute);
        event.getStartCalendar().set(Calendar.DAY_OF_MONTH, newDate.getDay());
        event.getStartCalendar().set(Calendar.MONTH, newDate.getMonth() - 1);
        event.getStartCalendar().set(Calendar.YEAR, newDate.getYear());
        event.setUptime(uptime);

        eventManager.updateEvent(event);
        updateEventContentRender(event);
    }
}
