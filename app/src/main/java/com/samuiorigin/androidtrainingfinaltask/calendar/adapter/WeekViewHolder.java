package com.samuiorigin.androidtrainingfinaltask.calendar.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

class WeekViewHolder {
    View view;
    ScrollView svEvents;
    LinearLayout layoutDaysEvent;
}
