package com.samuiorigin.androidtrainingfinaltask.calendar.adapter.listener;

import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.calendar.adapter.CalendarVPAdapter;
import com.samuiorigin.androidtrainingfinaltask.model.Event;
import com.samuiorigin.androidtrainingfinaltask.utils.IDate;

public class EventLayoutOnDragListener implements View.OnDragListener {
    private CalendarVPAdapter adapter;

    public EventLayoutOnDragListener(CalendarVPAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public boolean onDrag(View v, DragEvent dragEvent) {
        View _view = (View) dragEvent.getLocalState();
        switch (dragEvent.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_LOCATION:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                ((ViewGroup)_view.getParent()).removeView(_view);
                ((ViewGroup)v).addView(_view);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) _view.getLayoutParams();
                params.topMargin = (int) dragEvent.getY() - params.height/2;

                adapter.OnEventMoveEnded(
                        (Event) _view.getTag(),
                        params.topMargin,
                        (IDate) v.getTag());
                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                _view.setVisibility(View.VISIBLE);
                break;
        }
        return true;
    }
}
