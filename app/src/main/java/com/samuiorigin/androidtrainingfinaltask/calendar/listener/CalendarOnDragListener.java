package com.samuiorigin.androidtrainingfinaltask.calendar.listener;

import android.view.DragEvent;
import android.view.View;

import com.samuiorigin.androidtrainingfinaltask.calendar.CalendarFragment;

public class CalendarOnDragListener implements View.OnDragListener {
    private CalendarFragment calendarFragment;

    public CalendarOnDragListener(CalendarFragment calendarFragment) {
        this.calendarFragment = calendarFragment;
    }

    @Override
    public boolean onDrag(View v, DragEvent dragEvent) {
        switch (dragEvent.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                View view = (View) dragEvent.getLocalState();
                view.setVisibility(View.INVISIBLE);
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_LOCATION:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                break;
        }
        return false;
    }
}
