package com.samuiorigin.androidtrainingfinaltask.calendar.listener;

import android.support.v4.view.ViewPager;

import com.samuiorigin.androidtrainingfinaltask.calendar.CalendarFragment;
import com.samuiorigin.androidtrainingfinaltask.eventbus.Message;

import org.greenrobot.eventbus.EventBus;

public class CalendarOnPageChangeListener implements ViewPager.OnPageChangeListener {
    private CalendarFragment calendarFragment;

    public CalendarOnPageChangeListener(CalendarFragment calendarFragment) {
        this.calendarFragment = calendarFragment;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {
        EventBus.getDefault().post(new Message<>(
                Message.ACTION_UPDATE_DATE_IN_MENU,
                calendarFragment.adapter.getCurrentPageCalendar()
        ));
    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            if (calendarFragment.vpCalendar.getCurrentItem() == 0) {
                calendarFragment.adapter.setAndNotifyPreviousWeekData();
                calendarFragment.vpCalendar.setCurrentItem(1, false);
            } else if (calendarFragment.vpCalendar.getCurrentItem() == 2) {
                calendarFragment.adapter.setAndNotifyNextWeekData();
                calendarFragment.vpCalendar.setCurrentItem(1, false);
            }
        }
    }
}
