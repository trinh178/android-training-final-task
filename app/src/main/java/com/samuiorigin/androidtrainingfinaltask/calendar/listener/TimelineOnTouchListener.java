package com.samuiorigin.androidtrainingfinaltask.calendar.listener;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;

import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.calendar.CalendarFragment;
import com.samuiorigin.androidtrainingfinaltask.utils.Point;
import com.samuiorigin.androidtrainingfinaltask.utils.Utils;

public class TimelineOnTouchListener implements View.OnTouchListener {
    private CalendarFragment calendarFragment;

    public TimelineOnTouchListener(CalendarFragment calendarFragment) {
        this.calendarFragment = calendarFragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {
        if (motionEvent.getPointerCount() == 2) {
            switch (motionEvent.getActionMasked()) {
                case MotionEvent.ACTION_POINTER_DOWN:
                    v.setTag(R.id.touchDown1, new Point(
                            motionEvent.getX(0),
                            motionEvent.getY(0)
                    ));
                    v.setTag(R.id.touchDown2, new Point(
                            motionEvent.getX(1),
                            motionEvent.getY(1)
                    ));
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (Utils.angleBetween(
                            (Point) v.getTag(R.id.touchDown1),
                            (Point) v.getTag(R.id.touchDown2),
                            new Point(motionEvent.getX(0), motionEvent.getY(0))
                    ) >= 90 &&
                            Utils.angleBetween(
                                    (Point) v.getTag(R.id.touchDown2),
                                    (Point) v.getTag(R.id.touchDown1),
                                    new Point(motionEvent.getX(1), motionEvent.getY(1))
                            ) >= 90) {


                        // Zoom-in
                        calendarFragment.adapter.zoomIn(0.01f);


                    } else if (Utils.angleBetween(
                            (Point) v.getTag(R.id.touchDown1),
                            (Point) v.getTag(R.id.touchDown2),
                            new Point(motionEvent.getX(0), motionEvent.getY(0))
                    ) < 90 &&
                            Utils.angleBetween(
                                    (Point) v.getTag(R.id.touchDown2),
                                    (Point) v.getTag(R.id.touchDown1),
                                    new Point(motionEvent.getX(1), motionEvent.getY(1))
                            ) < 90) {


                        // Zoom-out
                        calendarFragment.adapter.zoomOut(0.01f);

                    }
                    v.setTag(R.id.touchDown1, new Point(
                            motionEvent.getX(0),
                            motionEvent.getY(0)
                    ));
                    v.setTag(R.id.touchDown2, new Point(
                            motionEvent.getX(1),
                            motionEvent.getY(1)
                    ));
                    break;
            }
        } else if (motionEvent.getPointerCount() == 1) {
            try {
                motionEvent.offsetLocation(0, 100);
                calendarFragment.vpCalendar.dispatchTouchEvent(motionEvent);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
        }
        return true;
    }
}
