package com.samuiorigin.androidtrainingfinaltask.dagger;

import dagger.Component;

@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(Object object);
}