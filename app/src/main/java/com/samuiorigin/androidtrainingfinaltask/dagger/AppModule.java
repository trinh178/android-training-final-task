package com.samuiorigin.androidtrainingfinaltask.dagger;

import com.samuiorigin.androidtrainingfinaltask.model.EventManager;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule {
    private EventBus eventBus;
    private EventManager eventManager;

    AppModule() {
        eventBus = EventBus.getDefault();
        eventManager = EventManager.getInstance();
    }

    @Provides
    EventBus provideEvenBus() {
        return eventBus;
    }
    @Provides
    EventManager provideEventManager() {
        return eventManager;
    }
}
