package com.samuiorigin.androidtrainingfinaltask.event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.base.BaseMvpActivity;
import com.samuiorigin.androidtrainingfinaltask.model.Event;
import com.samuiorigin.androidtrainingfinaltask.utils.Time;
import com.samuiorigin.androidtrainingfinaltask.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

@SuppressWarnings("unused")
public class EventActivity extends BaseMvpActivity<EventView, EventPresenter>
                           implements EventView, Validator.ValidationListener {
    @NotEmpty
    @Length(min = 10, max = 50)
    @BindView(R.id.etTitle)
    TextInputEditText etTitle;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvFrom) TextInputEditText tvFrom;
    @BindView(R.id.tvTo) TextInputEditText tvTo;
    @BindView(R.id.spAlertBefore) Spinner spAlertBefore;
    @NotEmpty
    @BindView(R.id.etLocation)
    TextInputEditText etLocation;
    @BindView(R.id.btnMode) ImageButton btnMode;
    @BindView(R.id.btnDelete) ImageButton btnDelete;

    @BindView(R.id.btnDate) View btnDate;
    @BindView(R.id.btnFrom) View btnFrom;
    @BindView(R.id.btnTo) View btnTo;

    private DatePickerDialog datePickerDialog;
    private TimePickerDialog fromTimePickerDialog;
    private TimePickerDialog toTimePickerDialog;

    private SpinnerAdapter alertBeforeAdapter;
    private ArrayList<Time> alertBeforeData;

    private boolean editMode = false;
    private Event event;

    // Saripaar
    private Validator validator;
    private boolean uptimeError = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        // Saripaar
        validator = new Validator(this);
        validator.setValidationListener(this);

        event = new Event(
                "",
                Calendar.getInstance(),
                new Time(1),
                new Time(1),
                ""
        );

        datePickerDialog = new DatePickerDialog(
                this,
                (view, year, month, dayOfMonth) -> {
                    tvDate.setText(Utils.dateToString(
                            getResources().getString(R.string.date_format_full),
                            dayOfMonth, month+1, year));
                    event.getStartCalendar().set(Calendar.YEAR, year);
                    event.getStartCalendar().set(Calendar.MONTH, month);
                    event.getStartCalendar().set(Calendar.DAY_OF_MONTH, dayOfMonth);
                },
                2019, 1, 1);
        fromTimePickerDialog = new TimePickerDialog(
                this,
                (view, hourOfDay, minute) -> {
                    tvFrom.setText(Utils.timeToString(
                            getResources().getString(R.string.time_format),
                            hourOfDay, minute, 0));
                    event.getStartCalendar().set(Calendar.HOUR_OF_DAY, hourOfDay);
                    event.getStartCalendar().set(Calendar.MINUTE, minute);

                    ValidateUpTime();
                },
                0, 0, true);
        toTimePickerDialog = new TimePickerDialog(
                this,
                (view, hourOfDay, minute) -> {
                    tvTo.setText(Utils.timeToString(
                            getResources().getString(R.string.time_format),
                            hourOfDay, minute, 0));
                    event.getEndCalendar().set(Calendar.HOUR_OF_DAY, hourOfDay);
                    event.getEndCalendar().set(Calendar.MINUTE, minute);

                    ValidateUpTime();
                },
                0, 0, true);

        alertBeforeDataInit();


        btnMode.setImageResource(editMode?R.drawable.ic_save:R.drawable.ic_edit);
        //btnDelete.setEnabled(isCreated);
    }

    private void ValidateUpTime() {
        if ((event.getStartCalendar().get(Calendar.HOUR_OF_DAY) * 100 +
                event.getStartCalendar().get(Calendar.MINUTE)) >=
                event.getEndCalendar().get(Calendar.HOUR_OF_DAY) * 100 +
                        event.getEndCalendar().get(Calendar.MINUTE)) {
            tvFrom.setError("Invalid");
            tvTo.setError("Invalid");
            uptimeError = true;
        } else {
            tvFrom.setError(null);
            tvTo.setError(null);
            uptimeError = false;
        }
    }

    @OnClick(R.id.btnDate)
    public void pickDate() {
        datePickerDialog.show();
    }
    @OnClick(R.id.btnFrom)
    public void pickFromTime() {
        fromTimePickerDialog.show();
    }
    @OnClick(R.id.btnTo)
    public void pickToTime() {
        toTimePickerDialog.show();
    }

    @OnClick(R.id.btnMode)
    public void switchMode(ImageButton btnMode) {
        validator.validate();
    }
    @OnClick(R.id.btnDelete)
    public void delete() {
        presenter.onDeleteClick();
    }

    //
    private void alertBeforeDataInit() {
        alertBeforeData = new ArrayList<>();

        alertBeforeData.add(new Time(1, 0));
        alertBeforeData.add(new Time(1, 30));
        alertBeforeData.add(new Time(2, 0));
        alertBeforeData.add(new Time(2, 30));

        alertBeforeAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                alertBeforeData);

        spAlertBefore.setAdapter(alertBeforeAdapter);
    }
    private int alertBeforeDataAdd(Time time) {
        int i = alertBeforeDataFind(time);
        if (i != -1) {
            i = alertBeforeData.size();
            alertBeforeData.add(time);
        }
        return i;
    }
    private int alertBeforeDataFind(Time time) {
        for (int i = 0; i < alertBeforeData.size(); i ++) {
            if (alertBeforeData.get(i).equals(time)) {
                return i;
            }
        }
        return -1;
    }

    // Mvp
    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }
    @NonNull
    @Override
    public EventPresenter createPresenter() {
        return new EventPresenter();
    }
    @Override
    public void showEvent(Event event) {
        this.event = event;
        etTitle.setText(event.getTitle());
        tvDate.setText(Utils.dateToString(
                getResources().getString(R.string.date_format_full), event.getDate()));
        tvFrom.setText(Utils.timeToString(
                getResources().getString(R.string.time_format), event.getStartTime()));
        tvTo.setText(Utils.timeToString(
                getResources().getString(R.string.time_format), event.getEndTime()));
        int i = alertBeforeDataAdd(event.getAlertBeforeTime());
        spAlertBefore.setSelection(i);
        etLocation.setText(event.getLocation());
    }
    @Override
    public void enableEditMode(boolean enable) {
        editMode = enable;

        etTitle.setEnabled(editMode);
        spAlertBefore.setEnabled(editMode);
        etLocation.setEnabled(editMode);

        btnDate.setEnabled(editMode);
        btnFrom.setEnabled(editMode);
        btnTo.setEnabled(editMode);

        btnMode.setImageResource(editMode?R.drawable.ic_save:R.drawable.ic_edit);
    }
    @Override
    public void close() {
        finish();
    }

    // Saripaar
    @Override
    public void onValidationSucceeded() {
        if (!uptimeError) {
            event.setTitle(Objects.requireNonNull(etTitle.getText()).toString());
            event.setAlertBeforeTime((Time) spAlertBefore.getSelectedItem());
            event.setLocation(Objects.requireNonNull(etLocation.getText()).toString());

            presenter.onModeClick(event);
        }
    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        errors.get(0).getView().requestFocus();
        for (ValidationError error: errors) {
            View view = error.getView();
            if (view instanceof TextInputEditText) {
                ((TextInputEditText) view).setError(error.getCollatedErrorMessage(this));
            }
        }
    }
}
