package com.samuiorigin.androidtrainingfinaltask.event;

import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.samuiorigin.androidtrainingfinaltask.eventbus.Message;
import com.samuiorigin.androidtrainingfinaltask.model.Event;
import com.samuiorigin.androidtrainingfinaltask.model.EventManager;
import com.samuiorigin.androidtrainingfinaltask.utils.Time;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;
import java.util.Objects;

@SuppressWarnings("unused")
public class EventPresenter extends MvpBasePresenter<EventView> {

    private Event event;

    private boolean isCreated;
    private boolean editMode;

    private void createEvent() {
        event = EventManager.getInstance().addEvent(event);
        Log.d("TEST", event.getStartCalendar().getTime().toString());
        EventBus.getDefault().postSticky(new Message<>(
                Message.ACTION_CREATED_EVENT,
                event
        ));
    }
    private void updateEvent() {
        EventBus.getDefault().postSticky(new Message<>(
                Message.ACTION_UPDATED_EVENT,
                event
        ));
        EventManager.getInstance().updateEvent(event);
    }
    private void deleteEvent() {
        EventBus.getDefault().postSticky(new Message<>(
                Message.ACTION_DELETED_EVENT,
                event
        ));
        EventManager.getInstance().removeEvent(event);
    }
    void onModeClick(Event e) {
        if (editMode) {
            event.setTitle(e.getTitle());
            event.setStartCalendar(e.getStartCalendar());
            event.setUptime(e.getUptime());
            event.setAlertBeforeTime(e.getAlertBeforeTime());
            event.setLocation(e.getLocation());

            if (!isCreated) {
                createEvent();
                isCreated = true;
            } else {
                updateEvent();
            }
        }
        editMode = !editMode;
        Objects.requireNonNull(getView()).enableEditMode(editMode);
    }
    void onDeleteClick() {
        deleteEvent();
        Objects.requireNonNull(getView()).close();
    }
    // Event bus
    void onResume() {
        EventBus.getDefault().register(this);
    }
    void onPause() {
        EventBus.getDefault().unregister(this);
    }
    @Subscribe(sticky = true)
    public void onEvent(Message msg) {
        if (msg.getAction() == Message.ACTION_DETAILS_EVENT) {
            event = (Event) msg.getData();
            isCreated = true;
            Objects.requireNonNull(getView()).showEvent(event);
            editMode = false;
        } else if (msg.getAction() == Message.ACTION_CREATE_EVENT) {
            event = new Event(
                    "",
                    Calendar.getInstance(),
                    new Time(1),
                    new Time(1),
                    ""
            );
            isCreated = false;
            Objects.requireNonNull(getView()).showEvent(event);
            editMode = true;
        }
        Objects.requireNonNull(getView()).enableEditMode(editMode);
    }
}
