package com.samuiorigin.androidtrainingfinaltask.event;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.samuiorigin.androidtrainingfinaltask.model.Event;

public interface EventView extends MvpView {
    void showEvent(Event event);
    void enableEditMode(boolean enable);
    void close();
}