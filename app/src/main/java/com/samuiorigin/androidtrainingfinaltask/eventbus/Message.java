package com.samuiorigin.androidtrainingfinaltask.eventbus;

@SuppressWarnings("unused")
public class Message<T> {
    // Constants
    public static final int ACTION_FOCUS_NOW_TIME = 0;
    public static final int ACTION_PREVIOUS_WEEK = 1;
    public static final int ACTION_NEXT_WEEK = 2;
    public static final int ACTION_UPDATE_DATE_IN_MENU = 3;
    public static final int ACTION_CREATE_EVENT = 4;
    public static final int ACTION_CREATED_EVENT = 5;
    public static final int ACTION_DETAILS_EVENT = 6;
    public static final int ACTION_DELETED_EVENT = 7;
    public static final int ACTION_UPDATED_EVENT = 8;



    private int action;
    private T data;

    public Message(int action) {
        this.action = action;
    }
    public Message(int id, T data) {
        this.action = id;
        this.data = data;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int id) {
        this.action = id;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
