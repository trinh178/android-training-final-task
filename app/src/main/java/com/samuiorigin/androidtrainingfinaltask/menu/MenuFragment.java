package com.samuiorigin.androidtrainingfinaltask.menu;

import android.support.annotation.NonNull;
import android.widget.TextView;

import com.samuiorigin.androidtrainingfinaltask.R;
import com.samuiorigin.androidtrainingfinaltask.base.BaseMvpFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class MenuFragment extends BaseMvpFragment<MenuView, MenuPresenter>
                          implements MenuView {
    //@BindView(R.id.btnCurrentDate) Button btnCurrentDate;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_menu;
    }

    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvToday) TextView tvToday;

    @OnClick(R.id.btnToday)
    void showCurrentDate() {
        presenter.onTodayClick();
    }
    @OnClick(R.id.btnPrevious)
    void previousPage() {
        presenter.onPreviousMonthClick();
    }
    @OnClick(R.id.btnNext)
    void nextPage() {
        presenter.onNextMonthClick();
    }
    @OnClick(R.id.btnCreateEvent)
    void createEvent() {
        presenter.onCreateEventClick();
    }

    // Mvp
    @NonNull
    @Override
    public MenuPresenter createPresenter() {
        return new MenuPresenter();
    }
    @Override
    public void setTodayText(String text) {
        tvToday.setText(text);
    }
    @Override
    public void setCalendarText(String text) {
        tvDate.setText(text);
    }

    // Event bus
    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }
}
