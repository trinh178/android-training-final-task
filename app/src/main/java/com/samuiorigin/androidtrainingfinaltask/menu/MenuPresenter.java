package com.samuiorigin.androidtrainingfinaltask.menu;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.samuiorigin.androidtrainingfinaltask.eventbus.Message;
import com.samuiorigin.androidtrainingfinaltask.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;
import java.util.Objects;

import javax.inject.Inject;

import static com.samuiorigin.androidtrainingfinaltask.eventbus.Message.ACTION_UPDATE_DATE_IN_MENU;

@SuppressWarnings("unused")
public class MenuPresenter extends MvpBasePresenter<MenuView> {

    @Inject
    EventBus eventBus;

    void onTodayClick() {
        EventBus.getDefault().post(new Message(Message.ACTION_FOCUS_NOW_TIME));
    }
    void onPreviousMonthClick() {
        EventBus.getDefault().post(new Message(Message.ACTION_PREVIOUS_WEEK));
    }
    void onNextMonthClick() {
        EventBus.getDefault().post(new Message(Message.ACTION_NEXT_WEEK));
    }
    void onCreateEventClick() {
        EventBus.getDefault().postSticky(new Message(Message.ACTION_CREATE_EVENT));
    }

    // Event bus
    void onResume() {
        if (isViewAttached()) {
            Calendar cal = Calendar.getInstance();
            Objects.requireNonNull(getView()).setTodayText(Utils.dateToString("dd-MM-yyyy", cal));
        }
        EventBus.getDefault().register(this);
    }
    void onPause() {
        EventBus.getDefault().unregister(this);
    }
    @Subscribe
    public void onEvent(Message msg) {
        switch (msg.getAction()) {
            case ACTION_UPDATE_DATE_IN_MENU:
                if (isViewAttached()) {
                    Calendar cal = (Calendar) msg.getData();
                    Objects.requireNonNull(getView()).setCalendarText(cal.get(Calendar.MONTH) + 1 + "-" + cal.get(Calendar.YEAR));
                }
                break;
        }
    }
}
