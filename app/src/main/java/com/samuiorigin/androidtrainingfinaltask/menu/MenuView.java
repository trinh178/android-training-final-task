package com.samuiorigin.androidtrainingfinaltask.menu;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface MenuView extends MvpView {
    void setTodayText(String text);
    void setCalendarText(String text);
}
