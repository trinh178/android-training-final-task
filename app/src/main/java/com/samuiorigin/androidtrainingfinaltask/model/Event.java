package com.samuiorigin.androidtrainingfinaltask.model;

import com.samuiorigin.androidtrainingfinaltask.utils.IDate;
import com.samuiorigin.androidtrainingfinaltask.utils.Time;
import com.samuiorigin.androidtrainingfinaltask.utils.Utils;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class Event extends RealmObject {
    private String title;

    @Ignore
    private Calendar startCalendar;
    private Date startDate;

    @Ignore
    private Calendar endCalendar;
    private Date endDate;

    @Ignore
    private Time alertBeforeTime;
    private Date alertDate;

    private String location;

    // Constructors
    public Event() {
    }
    public Event(String title, Calendar startCalendar, Time uptime, Time alertBeforeTime, String location) {
        this.title = title;
        this.startCalendar = startCalendar;
        this.endCalendar = (Calendar) startCalendar.clone();
        this.endCalendar.add(Calendar.HOUR_OF_DAY, uptime.getHour());
        this.endCalendar.add(Calendar.MINUTE, uptime.getMinute());
        this.alertBeforeTime = alertBeforeTime;
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (getRealm() != null) {
            getRealm().beginTransaction();
            this.title = title;
            getRealm().commitTransaction();
        } else {
            this.title = title;
        }
    }

    public IDate getDate() {
        return Utils.getIDate(startCalendar);
    }

    public Calendar getStartCalendar() {
        return startCalendar;
    }

    public Calendar getEndCalendar() {
        return endCalendar;
    }

    public void setStartCalendar(Calendar startCalendar) {
        this.startCalendar = startCalendar;
    }

    public Time getStartTime() {
        return Utils.getTime(startCalendar);
    }
    public Time getEndTime() {
        return Utils.getTime(endCalendar);
    }

    public Time getUptime() {
        return new Time(
                endCalendar.get(Calendar.HOUR_OF_DAY) - startCalendar.get(Calendar.HOUR_OF_DAY),
                endCalendar.get(Calendar.MINUTE) - startCalendar.get(Calendar.MINUTE)
        );
    }

    public void setUptime(Time uptime) {
        this.endCalendar.set(Calendar.HOUR_OF_DAY,
                this.startCalendar.get(Calendar.HOUR_OF_DAY));
        this.endCalendar.set(Calendar.MINUTE,
                this.startCalendar.get(Calendar.MINUTE));
        this.endCalendar.add(Calendar.HOUR_OF_DAY, uptime.getHour());
        this.endCalendar.add(Calendar.MINUTE, uptime.getMinute());
    }

    public Time getAlertBeforeTime() {
        return alertBeforeTime;
    }

    public void setAlertBeforeTime(Time alertBeforeTime) {
        this.alertBeforeTime = alertBeforeTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        if (getRealm() != null) {
            getRealm().beginTransaction();
            this.location = location;
            getRealm().commitTransaction();
        } else {
            this.location = location;
        }
    }

    // Realm
    void beforeCopyToRealm() {
        startDate = startCalendar.getTime();
        endDate = endCalendar.getTime();
        alertDate = Utils.getDate(alertBeforeTime);
    }
    void afterQueryFromRealm() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        startCalendar = cal;

        cal = Calendar.getInstance();
        cal.setTime(endDate);
        endCalendar = cal;

        cal = Calendar.getInstance();
        cal.setTime(alertDate);
        alertBeforeTime = Utils.getTime(cal);
    }
}
