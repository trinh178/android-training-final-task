package com.samuiorigin.androidtrainingfinaltask.model;

import android.annotation.SuppressLint;
import android.util.Log;

import com.samuiorigin.androidtrainingfinaltask.utils.IDate;
import com.samuiorigin.androidtrainingfinaltask.utils.Time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class EventManager {
    private static EventManager instance;
    public static EventManager getInstance() {
        if (instance == null) {
            instance = new EventManager();
        }
        return instance;
    }

    //
    private Map<String, List<Event>> hashMap;

    private DateFormat dateFormat;

    private Realm realm;

    @SuppressLint({"SimpleDateFormat","unused"})
    private EventManager() {
        hashMap = new HashMap<>();
        dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        realm = Realm.getDefaultInstance();
        //
        /*addEvent(new Event(
                "Have a meeting with customer from Singapore",
                new GregorianCalendar(2019, Calendar.APRIL, 22, 12, 30),
                new Time(1, 30),
                new Time(1),
                "BEESIGHT SOFT"
        ));
        addEvent(new Event(
                "Have a meeting with customer from China",
                new GregorianCalendar(2019, Calendar.APRIL, 23,13, 0),
                new Time(1),
                new Time(1),
                "BEESIGHT SOFT"
        ));
        addEvent(new Event(
                "Have a meeting with customer from Japan",
                new GregorianCalendar(2019, Calendar.APRIL, 23,15, 0),
                new Time(1),
                new Time(1),
                "BEESIGHT SOFT"
        ));
        addEvent(new Event(
                "Have a meeting with customer from US",
                new GregorianCalendar(2019, Calendar.MAY, 23,15, 0),
                new Time(1),
                new Time(1),
                "BEESIGHT SOFT"
        ));*/

        Event e = new Event(
                "Have a meeting with customer from Singapore",
                new GregorianCalendar(2019, Calendar.APRIL, 22, 12, 30),
                new Time(1, 30),
                new Time(1),
                "BEESIGHT SOFT"
        );
        Log.d("TEST", e.getStartCalendar().getTime().toString());

        //addEventToMap(e);

        /*realm.beginTransaction();
        e.beforeCopyToRealm();
        realm.copyToRealm(e);
        realm.commitTransaction();*/

        loadData();
    }

    private void loadData() {
        RealmQuery<Event> query = realm.where(Event.class);
        RealmResults<Event> results = query.findAll();
        for (Event event: results) {
            event.afterQueryFromRealm();
            addEventToMap(event);
        }
    }

    private void addEventToMap(Event event) {
        String key = event.getDate().toString(dateFormat);

        List<Event> list = hashMap.get(key);
        if (list == null) {
            list = new ArrayList<>();
            hashMap.put(key, list);
        }

        list.add(event);
    }
    private Event addEventToDb(Event event) {
        realm.beginTransaction();
        event.beforeCopyToRealm();
        event = realm.copyToRealm(event);
        realm.commitTransaction();
        event.afterQueryFromRealm();
        return event;
    }
    public Event addEvent(Event event) {
        // Check
        // TODO
        //
        addEventToMap(event);
        return addEventToDb(event);
    }

    private void removeEventFromMap(Event event) {
        String key = event.getDate().toString(dateFormat);

        List<Event> list = hashMap.get(key);
        if (list != null) {
            list.remove(event);
        }
    }
    private void removeEventFromDb(Event event) {
        realm.beginTransaction();
        event.deleteFromRealm();
        realm.commitTransaction();
    }
    public void removeEvent(Event event) {
        removeEventFromMap(event);
        removeEventFromDb(event);
    }

    private void updateEventFromMap(Event event) {
        for (String key: hashMap.keySet()) {
            List<Event> list = hashMap.get(key);
            if (list != null && list.contains(event)) {
                if (!event.getDate().toString(dateFormat).equals(key)) {
                    list.remove(event);
                    addEvent(event);
                    return;
                }
            }
        }
    }
    private void updateEventFromDb(Event event) {

    }
    public void updateEvent(Event event) {
        updateEventFromMap(event);
        updateEventFromDb(event);
    }

    public Event[] getEvents(IDate date) {
        String key = date.toString(dateFormat);

        List<Event> list = hashMap.get(key);
        if (list == null) {
            return new Event[0];
        }
        return list.toArray(new Event[0]);
    }
}
