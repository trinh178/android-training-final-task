package com.samuiorigin.androidtrainingfinaltask.utils;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class IDate {
    private int day;
    private int month;
    private int year;

    public IDate(int month, int year) {
        this.day = 1;
        this.month = month;
        this.year = year;
    }
    public IDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj.getClass() == this.getClass()) {
            IDate date = (IDate) obj;
            return date.day == this.day &&
                    date.month == this.month &&
                    date.year == this.year;
        }
        return false;
    }

    public String toString(DateFormat dateFormat) {
        Calendar cal = new GregorianCalendar(year, month - 1, day);
        return dateFormat.format(cal.getTime());
    }
}
