package com.samuiorigin.androidtrainingfinaltask.utils;

public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time() {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }
    public Time(int hour) {
        this.hour = hour;
        this.minute = 0;
        this.second = 0;
    }
    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
        this.second = 0;
    }
    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj.getClass() == this.getClass()) {
            Time time = (Time) obj;
            return time.hour == this.hour &&
                    time.minute == this.minute &&
                    time.second == this.second;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.valueOf(hour) + ":" + String.valueOf(minute);
    }

    //
    public boolean equalsMinuteLevel(Time time) {
        if (time == null) return false;
        return time.hour == this.hour &&
                time.minute == this.minute;
    }

    public float getFloatHours() {
        return hour + minute / 60f;
    }
}
