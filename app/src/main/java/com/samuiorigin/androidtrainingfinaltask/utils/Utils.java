package com.samuiorigin.androidtrainingfinaltask.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {
    public static int differenceBetweenDates(Calendar cal1, Calendar cal2) {
        Calendar c1 = (Calendar) cal1.clone();
        Calendar c2 = (Calendar) cal2.clone();
        c1.set(Calendar.HOUR_OF_DAY, 0);
        c1.set(Calendar.MINUTE, 0);
        c1.set(Calendar.SECOND, 0);
        c1.set(Calendar.MILLISECOND, 0);
        c2.set(Calendar.HOUR_OF_DAY, 0);
        c2.set(Calendar.MINUTE, 0);
        c2.set(Calendar.SECOND, 0);
        c2.set(Calendar.MILLISECOND, 0);

        long dif = c2.getTimeInMillis() - c1.getTimeInMillis();
        return (int) Math.floor(dif / (1000f*60*60*24));
    }
    public static Time getTime(Calendar cal) {
        return new Time(
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND));
    }
    public static Date getDate(Time time) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, time.getHour());
        cal.set(Calendar.MINUTE, time.getMinute());
        cal.set(Calendar.SECOND, time.getSecond());
        return cal.getTime();
    }
    public static IDate getIDate(Calendar cal) {
        return new IDate(
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.MONTH) + 1,
                cal.get(Calendar.YEAR)
        );
    }
    public static Time combineTime(Time time1, Time time2) {
        return null; // TODO
    }

    // Find angle between three point with center point
    public static double angleBetween(Point center, Point current, Point previous) {
        return Math.abs(Math.toDegrees(Math.atan2(current.x - center.x,current.y - center.y)-
                Math.atan2(previous.x- center.x,previous.y- center.y)));
    }

    // Format
    public static String dateToString(
            String format, int day, int month, int year) {
        String d = String.valueOf(day);
        if (d.length() == 1) d = "0" + d;
        String m = String.valueOf(month);
        if (m.length() == 1) m = "0" + m;
        format = format.replace("dd", d);
        format = format.replace("MM", m);
        format = format.replace("yyyy", String.valueOf(year));
        return format;
    }
    public static String dateToString(String format, IDate date) {
        return dateToString(format, date.getDay(), date.getMonth(), date.getYear());
    }
    public static String dateToString(String format, Calendar cal) {
        return dateToString(format,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.MONTH) + 1,
                cal.get(Calendar.YEAR));
    }
    public static String timeToString(
            String format, int hour, int minute, int second) {
        String h = String.valueOf(hour);
        if (h.length() == 1) h = "0" + h;
        String m = String.valueOf(minute);
        if (m.length() == 1) m = "0" + m;
        String s = String.valueOf(second);
        if (s.length() == 1) s = "0" + s;
        format = format.replace("hh", h);
        format = format.replace("mm", m);
        format = format.replace("ss", s);
        return format;
    }
    public static String timeToString(String format, Time time) {
        return timeToString(format, time.getHour(), time.getMinute(), time.getSecond());
    }
    public static String timeToString(String format, Calendar cal) {
        return timeToString(format,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND));
    }
}
